import subprocess
import sys
import gitlab

def cherry_pick(repo_url, target_branch, source_branch):
    subprocess.run(["git", "checkout", "-b", target_branch])
    subprocess.run(["git", "fetch", "origin", source_branch])

    commit_ids = subprocess.check_output(["git", "log", "--pretty=format:%H", f"origin/{source_branch}"]).decode().split("\n")

    for commit_id in reversed(commit_ids):
        result = subprocess.run(["git", "cherry-pick", commit_id])
        if result.returncode != 0:
            print(f"Error occurred while cherry-picking commit {commit_id}:")
            print(result.stderr)
            sys.exit(1)
    print("Cherry-picked commits successfully.")
    push_commits(target_branch)

def push_commits(target_branch):
        result=subprocess.run(["git", "push", "origin", target_branch])
        if result.returncode != 0:
            print(f"Error occurred while pushing the commits")
            print(result.stderr)
            sys.exit(1)

repo_url = "https://gitlab.com/SowmiyaBalaji/cherry-pick"
target_branch = "temporary"
source_branch = "develop"

cherry_pick(repo_url, target_branch, source_branch)
